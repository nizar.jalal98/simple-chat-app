# simple-chat-app
[click to start chatting](http://52.4.59.251:9000/)

> 

## About

Simple chat app using the most powerful framework for real-time applications  [Feathers](http://feathersjs.com).

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/simple-chat
    npm install
    ```

3. Start your app

    ```
    npm start
    ```



- The console will log the port that the app running on it [localhost](http://localhost:3030/)

- Open two browser's window to check real time dependability
